<?php
namespace db\seeder;

use Illuminate\Database\Seeder;


class users_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create default simple user
        $user = new \App\Models\User([
            'first_name' => 'Александр',
            'last_name' => 'Мазур',
            'is_admin' => 0,
            'is_author' => 0
        ]);
        $user->save();
        $email = $user->emails()->create([
            'email' => 'alexandrSimpleUser@ks.at',
            'is_main' => 1,
            'user_id' => $user->id
        ]);

        $email->password()->create([
             'email_id' => $email->id,
             'password' => bcrypt('30061996qQ')
        ]);

        //create default admin user
        $user = new \App\Models\User([
            'first_name' => 'Александр',
            'last_name' => 'Мазур',
            'is_admin' => 1,
            'is_author' => 0
        ]);
        $user->save();
        $email = $user->emails()->create([
            'email' => 'alexandrAdminUser@ks.at',
            'is_main' => 1,
            'user_id' => $user->id
        ]);

        $email->password()->create([
             'email_id' => $email->id,
             'password' => bcrypt('30061996qQ')
        ]);
    }
}
