<?php

use Illuminate\Database\Seeder;
use db\seeder\users_seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(users_seeder::class);
         $this->command->info('Users seeded!');
    }
}
