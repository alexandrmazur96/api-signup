<?php

namespace App\Http\Controllers\Api;

use App;
use App\Models\User;
use App\Services\MailProducer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use PhpAmqpLib\Connection\AMQPConnection;
use Validator;


class SignUpController extends Controller
{
    /**
     * Registrate user in system and send registration mail.
     */
    public function signUpUser(Request $request)
    {
        $requestParams = $request->only(
            'email',
            'first_name',
            'last_name',
            'password'
        );

        $email = ['email' => $request->input('email')];

        $requestValidationParams = [
            'email' => 'required|email|min:6',
            'first_name' => 'max:50|min:3',
            'last_name' => 'max:50|min:3',
            'password' => 'required|min:8'
        ];

        $emailExistsValidationParam = [
            'email' => 'unique:emails,email'
        ];

        $emailExistsValidator = Validator::make(
            $email,
            $emailExistsValidationParam
        );

        $requestParamsValidator = Validator::make(
            $requestParams,
            $requestValidationParams
        );

        if ($requestParamsValidator->fails()) {
            return response('Invalid params passed', 400);
        }

        if ($emailExistsValidator->fails()) {
            return response('User exists', 302);
        }

        $userParams = [
            'first_name' => $requestParams['first_name'],
            'last_name' => $requestParams['last_name'],
        ];

        $user = new User($userParams);
        $user->save();

        $email = $user->emails()->create([
            'user_id' => $user->id,
            'email' => $requestParams['email'],
            'is_main' => true
        ]);

        $email->password()->create([
            'email_id' => $email->id,
            'password' => bcrypt($requestParams['password'])
        ]);

        (new MailProducer)->produceSignUp($user->first_name, $email->email, $requestParams['password']);

        return response('User registered', 200);
    }
}
